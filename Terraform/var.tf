variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "eu-west-1"
}

variable "aws_access_key" {
 type = string
}

variable "aws_secret_key" {
  type = string
}

variable "aws_public_key" {
  type = string
}

variable "aws_zone" {
  default = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
}

variable "aws_name" {
  description = "AWS name of servers."
  default     = "Bart Simpson"
}

variable "aws_user_debian" {
  default = "admin"
}

variable "aws_owner" {
  default = "SwakTeam"
}

variable "aws_ec2_ami" {
  default = "ami-00a845a11b3d1f308"
}

variable "aws_ec2_type" {
  default = "t2.micro"
}

variable "aws_rds_username" {
  type = string
}

variable "aws_rds_password" {
  type = string
}