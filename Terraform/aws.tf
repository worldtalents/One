provider "aws" {
  profile = "default"
  region  = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_key_pair" "bart_simpson" {
  key_name = "swak"
//  public_key = file("~/.ssh/id_rsa.pub")
  public_key = var.aws_public_key
}

resource "aws_security_group" "homer_simpson" {
  ingress {
    from_port = 80
    protocol = "tcp"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 8080
    protocol = "tcp"
    to_port = 8080
    cidr_blocks = [
      for val in aws_instance.bart_simpson:
            "${val.private_ip}/32"
    ]
  }
}



resource "aws_security_group" "lisa_simpson" {
  ingress {
    from_port = 5432
    protocol = "tcp"
    to_port = 5432
    cidr_blocks = [
      for val in aws_instance.bart_simpson:
            "${val.private_ip}/32"
    ]
  }
  egress {
    from_port = 5432
    protocol = "tcp"
    to_port = 5432
    cidr_blocks = [
      for val in aws_instance.bart_simpson:
            "${val.private_ip}/32"
    ]
  }
}

resource "aws_security_group" "bart_simpson" {
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
  }

}

resource "aws_security_group_rule" "marge_simpson" {
  from_port = 8080
  protocol = "tcp"
  source_security_group_id = aws_security_group.homer_simpson.id
  security_group_id = aws_security_group.bart_simpson.id
  to_port = 8080
  type = "ingress"
}

resource "aws_instance" "bart_simpson" {
  ami = var.aws_ec2_ami
  instance_type = var.aws_ec2_type
  security_groups = [aws_security_group.bart_simpson.name]
  key_name = aws_key_pair.bart_simpson.key_name
  count = 4
}

resource "aws_elb" "homer_simpson" {
  availability_zones = var.aws_zone

  listener {
    instance_port = 8080
    instance_protocol = "http"
    lb_port = 80
    lb_protocol = "http"
  }

  instances = aws_instance.bart_simpson.*.id
  security_groups = [aws_security_group.homer_simpson.id]

  tags = {
    Name = "Swak ELB 1"
  }
}

resource "aws_db_instance" "bart_simpson" {
  instance_class = "db.t2.micro"
  allocated_storage = 20
  max_allocated_storage = 50
  engine = "postgres"
  engine_version = "11.5"
  name = "swak"
  identifier = "swak"
  username = var.aws_rds_username
  password = var.aws_rds_password
  vpc_security_group_ids = [aws_security_group.lisa_simpson.id]
  skip_final_snapshot = true
}


