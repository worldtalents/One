from web.app import User
from faker import Faker
from os import getenv

fake = Faker("fr_FR")


def test_hello(client):
    response = client.get('/')
    assert response.status_code == 201


def test_register(client):
    username = fake.email()
    password = fake.password()
    response = client.post('/registration', json={'username': username, "password": password})
    assert response.status_code == 200
    assert User.find_by_username(username).username == username

    error = client.post('/registration',
                        json={
                            'username': username,
                            "password": password
                            })
    assert error.status_code == 201

    init = client.post('/registration', json={'username': getenv('username'), "password": getenv('password')})

    assert init.status_code == 200


def test_login(client):
    username = fake.email()
    password = fake.password()

    response = client.post('/login', json={'username': 'vpelissi@student.42.fr', 'password': 'vertu1234'})
    error_not_found = client.post('/login', json={'username': username, 'password': password})
    error_password = client.post('/login', json={'username': 'vpelissi@student.42.fr', 'password': 'vertu123'})

    assert response.status_code == 200
    assert error_not_found.status_code == 401
    assert error_password.status_code == 401
