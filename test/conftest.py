from pytest import fixture
from web import app


@fixture()
def client():
    app.app.config['testing'] = True
    yield app.app.test_client()
